package com.example.camera;

import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.opengl.GLTextureView;
import com.example.camera.legacy.CameraCallback;
import com.example.camera.legacy.CameraThread;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //initializeView();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "App Started", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initializeView() {
        final CameraThread[] cameraThread = {null};

        GLTextureView textureView = findViewById(R.id.preview);
        textureView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                Log.d("LCI", "onSurfaceTextureAvailable, Starting Camera");

                Surface surface = new Surface(surfaceTexture);
                cameraThread[0] = new CameraThread(MainActivity.this, new CameraCallback(surface));

                Thread thread = new Thread(cameraThread[0], "Sonix");
                thread.start();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                Log.d("LCI", "onSurfaceTextureSizeChanged");
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                Log.d("LCI", "onSurfaceTextureDestroyed");
                cameraThread[0].shutdown();
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}