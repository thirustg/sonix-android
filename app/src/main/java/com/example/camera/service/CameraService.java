package com.example.camera.service;

import android.content.Context;
import android.util.Log;
import android.view.Surface;

import com.Hardware.Decoder;
import com.android.VideoCodec.VideoHeader;
import com.module.UDP;
import com.rearcam.RealCam;

public class CameraService implements RealCam.Callback {

    // TODO - Are we doing 1080p?
    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;
    // TODO - Why frame rate of 15?
    public static final int FRAME_RATE = 15;
    // TODO - What is this player state?
    public static final int PLAYER_STATE = 1;
    public static final String WEP_PASSWORD_ADMIN = "admin";
    public static final int TIMEOUT = 3000;
    public static final boolean LIBRARY_LOG_ENABLE = false;
    public static final boolean WEP_ENABLED = true;

    private Decoder decoder;
    private Surface surface;
    private Context context;

    private Decoder.Notify decoderNotify = new Decoder.Notify() {
        @Override
        public void onNotify(int notify) {
            Log.d("LCI", String.format("Decoder.Notify %d", notify));
            //TODO - When would this happen? Any other failure scenarios?
            if (Decoder.DECODE_DEQUEUE_INPUTBUFFER_ERROR == notify) {
                if (null != decoder) {
                    Log.i("LCI", "Stopping Decoder");
                    decoder.stopRunning();
                    decoder = null;
                }
            }
        }
    };

    public CameraService(Context context) {
        this.context = context;
    }

    public Surface getSurface() {
        return surface;
    }

    public void setSurface(Surface surface) {
        this.surface = surface;
    }

    public void initialize() {

        try {
            //TODO - What is this delay and BEACON_DATA means?
            RealCam.setHeartBeatSleep(RealCam.mSyncThreadSleep, RealCam.BEACON_DATA);

            RealCam.setWEP(WEP_ENABLED, WEP_PASSWORD_ADMIN.getBytes());

            if (!RealCam.isInit())
                RealCam.libInit(this.context, TIMEOUT);

            short port = (short) RealCam.getUdpPort();
            byte[] portArray = RealCam.intToByteShort(port);

            RealCam.sendStartCmd(portArray);
            RealCam.sendSyncCmd();

            RealCam.setLibraryLogEnable(LIBRARY_LOG_ENABLE);

            RealCam.setCallback(this);

        } catch(Exception ex) {
            Log.e("LCI", "Initializing Camera Failed", ex);
        }
    }

    public void shutdown() {
        RealCam.sendStopCmd();
        RealCam.stopSyncCmd();
        RealCam.stopStartCmd();
        RealCam.libUninit();
    }

    private void initializeDecoder(byte[] videoData) {

        final VideoHeader videoHeader = new VideoHeader();
        // TODO - What is this check?)
        if (videoData[4] == (byte) 0x67) {

            videoHeader.parseHeader(videoData);
            Log.d("LCI", String.format("SPS & PPS %s %s", new String(videoHeader.getSPS()), new String(videoHeader.getPPS())));

            Log.i("LCI", "Initializing Hardware Decoder");
            // TODO - What is playerState?
            decoder = new Decoder(surface, PLAYER_STATE, decoderNotify);

            // TODO - What is SPS/PPS, Frame Rate?
            decoder.initial(WIDTH, HEIGHT, FRAME_RATE, videoHeader.getSPS(), videoHeader.getPPS());
        } else {
            Log.d("LCI", String.format("initialize received invalid frame %d", videoData[4]));
            return;
        }
    }


    @Override
    public void onVideo(long timeStamp, byte[] videoData) {
        Log.d("LCI", String.format("onVideo %d %d", timeStamp, videoData.length));

        if (null == decoder) {
            initializeDecoder(videoData);
        }

        if (null != decoder) {
            decoder.decode(videoData);
        }
    }

    @Override
    public void onAudio(long l, byte[] bytes) {
        //TODO - Need to implement
        //Log.d("LCI", String.format("onAudio %d %d", l, bytes.length));
    }

    @Override
    public void onNotify(int opCode, byte[] bytes) {
        //TODO - Need to implement
        Log.d("LCI", String.format("onNotify %d %d", opCode, bytes.length));
    }

    @Override
    public void onFWInfo(byte[] bytes) {
        //TODO - Read the Firmware info?
        //Log.d("LCI", String.format("onFWInfo %d %s", bytes.length, new String(bytes)));
    }

    @Override
    public void onHeartBeatEvent(int event) {
        if (event == UDP.EVENT_STOP_RECEIVE) {
            Log.i("LCI", "App stopped receiving feed");
        } else if (event == UDP.EVENT_SOCKE_TIMEOUT) {
            Log.i("LCI", "Not Connected to WiFi");
        } else {
            Log.d("LCI", String.format("onHeartBeatEvent %d", event));
        }
    }

    //SONiX_7ABDCBC7033A
    @Override
    public void onUpdateFWPercent(int total, int now) {
        Log.d("LCI", String.format("onUpdateFWPercent %d %d", total, now));
    }
}
