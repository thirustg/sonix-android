package com.example.camera.legacy;

import android.app.Activity;
import android.util.Log;

import com.rearcam.RealCam;

public class CameraThread implements Runnable {

    Activity activity;
    RealCam.Callback callback;

    public CameraThread(Activity activity, RealCam.Callback callback) {
        this.activity = activity;
        this.callback = callback;
    }

    @Override
    public void run() {

        setupHeartBeat();
        initializeCamera();
    }

    public void initializeCamera() {

        try {
            RealCam.setWEP(true,"admin".getBytes());

            if (!RealCam.isInit())
                RealCam.libInit(activity, 3000);

            short port = (short) RealCam.getUdpPort();
            byte[] portArray = RealCam.intToByteShort(port);

            RealCam.sendStartCmd(portArray);
            RealCam.sendSyncCmd();

            RealCam.setLibraryLogEnable(false);

            if (null != callback) {
                RealCam.setCallback(callback);
            }
        } catch(Exception ex) {
            Log.e("LCI", "Initializing Camera Failed", ex);
        }
    }

    public void setupHeartBeat() {
        //TODO - What is this packets?
        byte[] heartBeatPacket = {0, 15, 28, 0, 7, 0, 64, 127, 83, 78, 57, 51, 53, 55, 50, 45, 45, 66, 85, 67, 45};
        //TODO - What is this delay means?
        RealCam.setHeartBeatSleep(RealCam.mSyncThreadSleep, RealCam.BEACON_DATA);
    }

    public void shutdown() {
        RealCam.sendStopCmd();
        RealCam.stopSyncCmd();
        RealCam.stopStartCmd();
        RealCam.libUninit();
    }
}