package com.example.camera.view;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

import com.android.opengl.GLTextureView;
import com.example.camera.service.CameraService;

public class CameraView extends GLTextureView implements TextureView.SurfaceTextureListener {

    CameraService cameraService;

    public CameraView(Context context) {
        super(context);
        initializeView(context);
    }

    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d("LCI", "ViewRenderer Constructor with Attributes");
        initializeView(context);
    }

    private void initializeView(Context context) {
        cameraService = new CameraService(context);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        setSurfaceTextureListener(this);
    }

    @Override
    public void onDestroy() {
        Log.d("LCI", "onDestroy");
        super.onDestroy();
        cameraService.shutdown();
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        Log.d("LCI", "onSurfaceTextureAvailable, Starting Camera");

        Surface surface = new Surface(surfaceTexture);
        cameraService.setSurface(surface);
        cameraService.initialize();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.d("LCI", "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.d("LCI", "onSurfaceTextureDestroyed, Shutting down Camera");
        cameraService.shutdown();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }
}
